const { shop } = require('../models')

module.exports = {
    list: async (req,res) =>{
        const data = await shop.findAll()
        if (data.length < 1) {
            return res.status(404).json({
                status:400,
                message:'data tidak ditemukan'
            })
        }
        res.status(200).json({
            status:200,
            message:'successfull',
            response:data
        })
    },

    new:async(req,res) =>{
        const {name="", author="", approved=false } = req.body
        if (name == "") {
            return res.status(400).json({
                status:400,
                message:'name tidak boleh kosong'
            })
        }
        if (author == "") {
            return res.status(400).json({
                status:400,
                message:'author tidak boleh kosong'
            })
        } else {
            const create = await shop.create({
                name:name,
                author:author,
                approved:approved
            })
            if (create == null) {
                return res.status(500).json({
                    status:500,
                    message:"server error"
                })
            }
            res.status(200).json({
                status:200,
                message:'successfull insert new data',
                response:create
            })
        }
    },

    update:async(req, res) => {
        const {shopId} = req.params
        const {name="", author="", approved=false } = req.body
        if (name == "") {
            return res.status(400).json({
                status:400,
                message:'name tidak boleh kosong'
            })
        }
        if (author == "") {
            return res.status(400).json({
                status:400,
                message:'author tidak boleh kosong'
            })
        } else {
            const update = await shop.update({
                name:name,
                author:author,
                approved:approved
            },{
                where:{
                    id:shopId
                }
            })
            if (update == null) {
                return res.status(500).json({
                    status:500,
                    message:"server error"
                })
            }
            res.status(200).json({
                status:200,
                message:'successfull update new data',
                response:update
            })
        }
    },

    delete: async(req, res) => {
        const {shopId} = req.params
        const del = await shop.destroy({
            where:{
                id:shopId
            }   
        })
        if (del == null) {
            return res.status(500).json({
                status:500,
                message:"server error"
            })
        }
        res.status(200).json({
            status:200,
            message:'successfull delete new data',
            response:del
        })
    }
    
}