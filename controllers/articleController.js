const { article } = require('../models')

module.exports = {
    list: async (req,res) => {
        const data = await article.findAll({
            where:{
                approved:true
            }
        })
        if (data.length == 0)  {
            return res.status(404).json({
                status:404,
                message:'data tidak ditemukan'
            })
        }
        res.status(200).json({
            status:200,
            message:'successfull',
            response:data
        })
    },

    new: async(req,res) =>{
        const {title="", body="", approved=false } = req.body
        if (title == "") {
            return res.status(400).json({
                status:400,
                message:'title tidak boleh kosong'
            })
        }
        if (body == "") {
            return res.status(400).json({
                status:400,
                message:'body tidak boleh kosong'
            })
        } else {
            const create = await article.create({
                title:title,
                body:body,
                approved:approved
            })
            if (create == null) {
                return res.status(500).json({
                    status:500,
                    message:"server error"
                })
            }
            res.status(200).json({
                status:200,
                message:'successfull insert new data',
                response:create
            })
        }
    },

    update:async(req, res) => {
        const {articleId} = req.params
        const {title="", body="", approved=false } = req.body
        if (title == "") {
            return res.status(400).json({
                status:400,
                message:'title tidak boleh kosong'
            })
        }
        if (body == "") {
            return res.status(400).json({
                status:400,
                message:'body tidak boleh kosong'
            })
        } else {
            const update = await article.update({
                title:title,
                body:body,
                approved:approved
            },{
                where:{
                    id:articleId
                }
            })
            if (update == null) {
                return res.status(500).json({
                    status:500,
                    message:"server error"
                })
            }
            res.status(200).json({
                status:200,
                message:'successfull update new data',
                response:update
            })
        }
    },

    delete:async(req, res) => {
        const {articleId} = req.params
        const del = await article.destroy({
            where:{
                id:articleId
            }   
        })
        if (del == null) {
            return res.status(500).json({
                status:500,
                message:"server error"
            })
        }
        res.status(200).json({
            status:200,
            message:'successfull delete new data',
            response:del
        })
    }


}