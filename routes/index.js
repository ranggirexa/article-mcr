const router = require('express').Router()
const articleRouter = require('./articleRouter')
const shopRouter = require('./shopRouter')

router.use(articleRouter, shopRouter)
// router.use(shopRouter)

module.exports = router