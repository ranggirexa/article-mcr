const routes = require('express').Router()
const shop = require('../controllers/shopController')

routes.get('/api/v0/shop', shop.list)

routes.post('/api/v0/shop', shop.new)

routes.put('/api/v0/shop/:shopId', shop.update)

routes.delete('/api/v0/shop/:shopId', shop.delete)

module.exports = routes
