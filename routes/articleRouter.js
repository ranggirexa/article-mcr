const routes = require('express').Router()
const article = require('../controllers/articleController')

routes.get('/api/v0/article', article.list)

routes.post('/api/v0/article', article.new)

routes.put('/api/v0/article/:articleId', article.update)

routes.delete('/api/v0/article/:articleId', article.delete)

module.exports = routes