const { response } = require('express')
const express = require('express')
const {article, shop} = require('./models')
const app = express()
const port = process.env.PORT || 2006
const router = require('./routes')

const swaggerUI = require('swagger-ui-express')
const swaggerJson = require('./docs/swagger.json')


app.listen(port, () =>{
    console.log(`server running at port ${port}`);
})

app.use(express.json())
app.get('/health-check', (request, response) => {
    response.json({
        message: 'health check sucess'
    })
})

app.use('/api/v0/docs', swaggerUI.serve, swaggerUI.setup(swaggerJson))

app.use(router)

// app.get('/api/v0/article',async (req,res) =>{
//     const data = await article.findAll()
//     if (data.length < 1) {
//         return res.status(404).json({
//             status:400,
//             message:'data tidak ditemukan'
//         })
//     }
//     res.status(200).json({
//         status:200,
//         message:'successfull',
//         response:data
//     })
// })

// app.post('/api/v0/article', async(req,res) =>{
//     const {title="", body="", approved=false } = req.body
//     if (title == "") {
//         return res.status(400).json({
//             status:400,
//             message:'title tidak boleh kosong'
//         })
//     }
//     if (body == "") {
//         return res.status(400).json({
//             status:400,
//             message:'body tidak boleh kosong'
//         })
//     } else {
//         const create = await article.create({
//             title:title,
//             body:body,
//             approved:approved
//         })
//         if (create == null) {
//             return res.status(500).json({
//                 status:500,
//                 message:"server error"
//             })
//         }
//         res.status(200).json({
//             status:200,
//             message:'successfull insert new data',
//             response:create
//         })
//     }
// })

// app.put('/api/v0/article/:articleId', async(req, res) => {
//     const {articleId} = req.params
//     const {title="", body="", approved=false } = req.body
//     if (title == "") {
//         return res.status(400).json({
//             status:400,
//             message:'title tidak boleh kosong'
//         })
//     }
//     if (body == "") {
//         return res.status(400).json({
//             status:400,
//             message:'body tidak boleh kosong'
//         })
//     } else {
//         const update = await article.update({
//             title:title,
//             body:body,
//             approved:approved
//         },{
//             where:{
//                 id:articleId
//             }
//         })
//         if (update == null) {
//             return res.status(500).json({
//                 status:500,
//                 message:"server error"
//             })
//         }
//         res.status(200).json({
//             status:200,
//             message:'successfull update new data',
//             response:update
//         })
//     }
// })
// app.delete('/api/v0/article/:articleId', async(req, res) => {
//     const {articleId} = req.params
//     const del = await article.destroy({
//         where:{
//             id:articleId
//         }   
//     })
//     if (del == null) {
//         return res.status(500).json({
//             status:500,
//             message:"server error"
//         })
//     }
//     res.status(200).json({
//         status:200,
//         message:'successfull delete new data',
//         response:del
//     })
// })

// app.get('/api/v0/shop',async (req,res) =>{
//     const data = await shop.findAll()
//     if (data.length < 1) {
//         return res.status(404).json({
//             status:400,
//             message:'data tidak ditemukan'
//         })
//     }
//     res.status(200).json({
//         status:200,
//         message:'successfull',
//         response:data
//     })
// })

// app.post('/api/v0/shop', async(req,res) =>{
//     const {name="", author="", approved=false } = req.body
//     if (name == "") {
//         return res.status(400).json({
//             status:400,
//             message:'name tidak boleh kosong'
//         })
//     }
//     if (author == "") {
//         return res.status(400).json({
//             status:400,
//             message:'author tidak boleh kosong'
//         })
//     } else {
//         const create = await shop.create({
//             name:name,
//             author:author,
//             approved:approved
//         })
//         if (create == null) {
//             return res.status(500).json({
//                 status:500,
//                 message:"server error"
//             })
//         }
//         res.status(200).json({
//             status:200,
//             message:'successfull insert new data',
//             response:create
//         })
//     }
// })

// app.put('/api/v0/shop/:shopId', async(req, res) => {
//     const {shopId} = req.params
//     const {name="", author="", approved=false } = req.body
//     if (name == "") {
//         return res.status(400).json({
//             status:400,
//             message:'name tidak boleh kosong'
//         })
//     }
//     if (author == "") {
//         return res.status(400).json({
//             status:400,
//             message:'author tidak boleh kosong'
//         })
//     } else {
//         const update = await shop.update({
//             name:name,
//             author:author,
//             approved:approved
//         },{
//             where:{
//                 id:shopId
//             }
//         })
//         if (update == null) {
//             return res.status(500).json({
//                 status:500,
//                 message:"server error"
//             })
//         }
//         res.status(200).json({
//             status:200,
//             message:'successfull update new data',
//             response:update
//         })
//     }
// })
// app.delete('/api/v0/shop/:shopId', async(req, res) => {
//     const {shopId} = req.params
//     const del = await shop.destroy({
//         where:{
//             id:shopId
//         }   
//     })
//     if (del == null) {
//         return res.status(500).json({
//             status:500,
//             message:"server error"
//         })
//     }
//     res.status(200).json({
//         status:200,
//         message:'successfull delete new data',
//         response:del
//     })
// })
